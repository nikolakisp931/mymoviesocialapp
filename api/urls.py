from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from api.views import user, movie

urlpatterns = [
    # JWT Token
    path('token', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh', TokenRefreshView.as_view(), name='token_refresh'),

    # User resource
    path('users', user.UserListCreate.as_view(), name='userListCreation'),
    path('users/<int:pk>', user.UserDetail.as_view(), name='userRetrieval'),

    # Movie resource
    path('movies', movie.MovieListCreate.as_view(), name='addMovie'),

    # Movie Vote resource
    path('movies/<int:movie_id>/votes', movie.MovieVoteListCreateUpdateDelete.as_view(),
         name='movieVote'),
]
