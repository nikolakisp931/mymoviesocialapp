from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken

from api.models import User, Movie, Vote


class MovieTests(APITestCase):
    """
    TestCase class that exercises the Movie API Resource
    """

    def setUp(self) -> None:
        """
        Initial test-suite setup
        """

        # Create users
        self.user1 = User.objects.create(first_name="Nikolas", last_name="Pap", username="nikolas",
                                         email="nikolas@test.com",
                                         password="Nikolas1")

        # Create movies
        self.movie1 = Movie.objects.create(title="MyMovie1", description="Desciption", user=self.user1)
        self.movie2 = Movie.objects.create(title="MyMovie2", description="Desciption2", user=self.user1)

        # Create Vote
        Vote.objects.create(movie=self.movie1, user=self.user1, reaction=Vote.SupportedMovieVotes.LIKE)

    def tearDown(self):
        """
        Handle end of test-runs
        """

        User.objects.all().delete()
        Movie.objects.all().delete()
        Vote.objects.all().delete()

    def test_movie_list(self):
        """
        Test GET: /api/movies/
        """

        url = reverse('addMovie')

        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

        user_fields = ('id', 'title', 'description', 'created', 'user', 'likes', 'hates', 'vote')
        self.assertTrue(all(k in response.data[0] for k in user_fields), 'Missing field from movie')

        # Make sure we get the correct ordering
        response = self.client.get(url + "?ordering=title", format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]["title"], self.movie1.title)
        self.assertEqual(response.data[1]["title"], self.movie2.title)


    def test_movie_create(self):
        """
        Test POST: /api/movies
        """

        url = reverse('addMovie')
        # authorized
        response = self.client.post(url, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Logged user movie creation
        token = RefreshToken.for_user(self.user1)
        self.client.credentials(HTTP_AUTHORIZATION="Bearer " + str(token.access_token))
        data = {
            "title": "Test Movie2"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Movie.objects.count(), 3)

        # Check user
        movie = Movie.objects.get(pk=response.data['id'])
        self.assertEqual(movie.user_id, self.user1.pk)