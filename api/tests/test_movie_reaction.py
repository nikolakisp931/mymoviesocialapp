from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken

from api.models import User, Movie, Vote


class MovieVoteTests(APITestCase):

    def setUp(self) -> None:
        """
        Initial test-suite setup
        """

        # Create users
        self.user1 = User.objects.create(first_name="Nikolas", last_name="Pap", username="nikolas",
                                         email="nikolas@test.com",
                                         password="Nikolas1")
        self.user2 = User.objects.create(first_name="Nikolas2", last_name="Pap2", username="nikolas2",
                                         email="nikolas2@test.com",
                                         password="Nikolas2")

        # Create movies
        self.movie1 = Movie.objects.create(title="MyMovie1", description="Desciption", user=self.user1)
        self.movie2 = Movie.objects.create(title="MyMovie2", description="Desciption2", user=self.user2)

        # Create Vote
        Vote.objects.create(movie=self.movie1, user=self.user1, reaction=Vote.SupportedMovieVotes.LIKE)
        Vote.objects.create(movie=self.movie2, user=self.user2, reaction=Vote.SupportedMovieVotes.HATE)

    def tearDown(self):
        """
        Handle end of test-runs
        """

        User.objects.all().delete()
        Movie.objects.all().delete()
        Vote.objects.all().delete()


    def test_movie_vote_create(self):
        """
        Test POST: /api/movies/<id>/votes
        """

        url = reverse('movieVote', kwargs={'movie_id': self.movie1.id})

        # Login as a user and create new vote
        token = RefreshToken.for_user(self.user2)
        self.client.credentials(HTTP_AUTHORIZATION="Bearer " + str(token.access_token))
        data = {
            "reaction": Vote.SupportedMovieVotes.LIKE
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Vote.objects.count(), 3)