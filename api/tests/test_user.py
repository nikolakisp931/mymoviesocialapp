from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from api.models import User


class UserTests(APITestCase):
    """
    TestCase class that exercises the User API Resource
    """

    def setUp(self) -> None:
        """
        Initial test-suite setup
        """

        # Create users
        self.user1 = User.objects.create(first_name="Nikolas", last_name="Pap", username="nikolas", email="nikolas@test.com",
                                         password="Nikolas1")

    def tearDown(self):
        """
        Handle end of test-runs
        """

        User.objects.all().delete()

    def test_user_list(self):
        """
        Test GET: /api/users/
        """

        url = reverse('userListCreation')

        # Make sure the endpoint is publicly accessible and returns the created user
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

        user_fields = ('id', 'first_name', 'last_name', 'username', 'email')
        self.assertTrue(all(k in response.data[0] for k in user_fields), 'Missing field from user')


    def user_creation(self):
        """
        Test POST: /api/users
        """

        url = reverse('userListCreation')

        # Make sure the endpoint is publicly accessible and creates a new user
        data = {
            "username": "nikolas",
            "first_name": "test1",
            "last_name": "test1l",
            "email": "test@test.com",
            "password": "Nikolas1"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 3)

        # Make sure we have all required fields
        response = self.client.post(url, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('username', response.data)
        self.assertIn('first_name', response.data)
        self.assertIn('last_name', response.data)
        self.assertIn('email', response.data)
        self.assertIn('password', response.data)
