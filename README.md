# MovieRama

The application is a social sharing platform where users can share their favorite movies. Each movie has a title
and a small description as well as a date that corresponds to the date it was added to the
database. In addition it holds a reference to the user that submitted it. Users can also
express their opinion about a movie by either likes or hates. The application uses a REST API 
and a web interface using Django Rest Framework and JSON Web token.
![alt text](homepage.png)

## Build/Deploy

After clone this repo
```
cd movierama
```
Build/Deploy using Docker

```
docker-compose up
```

Build/Deploy local (using start.sh bash script)

```
./start.sh
```

## ToDo

- Enhance test suite with more test cases for order/react
- UI Changes for table of movies
