FROM python:3.8-slim

WORKDIR /server

# Install python dependencies
RUN pip install --upgrade pip
RUN pip install pipenv

# Copy project artifacts
COPY . /server/

# Install project dependencies
RUN pipenv install --dev

EXPOSE 8000