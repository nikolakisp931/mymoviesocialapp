from django.urls import path, include

from drf_yasg2.views import get_schema_view
from drf_yasg2 import openapi

schema_view = get_schema_view(
    openapi.Info(
        title="Movierama",
        default_version='v1'
    ),
    public=True,
)

urlpatterns = [
    # MovieRama Backend routing
    path('api/', include('api.urls'))
]
